﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;

public class ProgressManager : MonoBehaviour {

	static ProgressManager _instance;
	static ProgressManager Instance
	{
		get
		{
			if (_instance == null)
			{
				new GameObject("ProgressManager", typeof(ProgressManager));
			}
			return _instance;
		}
	}

	Dictionary<string, bool> ownedItems;
	int coinsCache;

	void Awake()
	{
		if (_instance != null)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);

		coinsCache = PlayerPrefs.GetInt("current-coins", 0);

		ownedItems = new Dictionary<string, bool>();
		string itemJSON = PlayerPrefs.GetString("owned-items", "");
		if (itemJSON != "")
		{
			IList itemList = Json.Deserialize(itemJSON) as IList;
			foreach (string item in itemList)
			{
				if (!ownedItems.ContainsKey(item))
				{
					ownedItems.Add(item, true);
				}
			}
		}
	}

	public static bool PurchaseItem(string ID, int price)
	{
		if (price > CurrentCoins)
		{
			return false;
		}
		if (!Instance.ownedItems.ContainsKey(ID))
		{
			Instance.ownedItems.Add(ID, true);

			IList items = new System.Collections.ArrayList(Instance.ownedItems.Count);
			foreach(string item in Instance.ownedItems.Keys)
			{
				items.Add(item);
			}
			string itemsJSON = Json.Serialize(items);
			PlayerPrefs.SetString("owned-items", itemsJSON);
		}

		int coins = PlayerPrefs.GetInt("current-coins", 0);
		PlayerPrefs.SetInt("current-coins", coins - price);
		Instance.coinsCache = PlayerPrefs.GetInt("current-coins", 0);
		return true;
	}

	public static bool ItemOwned(string ID)
	{
		return Instance.ownedItems.ContainsKey(ID);
	}

	public static void AddCoins(int amount)
	{
		int coins = PlayerPrefs.GetInt("current-coins", 0);
		PlayerPrefs.SetInt("current-coins", coins + amount);
		Instance.coinsCache = PlayerPrefs.GetInt("current-coins", 0);
	}

	public static int CurrentCoins
	{
		get
		{
			return Instance.coinsCache;
		}
	}
}
