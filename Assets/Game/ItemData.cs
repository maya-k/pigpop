﻿using UnityEngine;
using System.Collections;

public class ItemData : ScriptableObject {

	[System.Serializable]
	public class Item
	{
		public string displayName;
		public string dataName;
		public Sprite sprite;
		public int cost;
		public float coinMultiplier;
	}

	public Item[] items;

	void OnEnable()
	{
		if (items != null)
		{
			System.Array.Sort(items, (a,b)=>a.cost.CompareTo(b.cost));
		}
	}
}
