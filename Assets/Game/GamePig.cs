﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class GamePig : MonoBehaviour, IPointerClickHandler {

	public int maxTaps = 3;
	public float balloonForce = 5;
	public float popForce = 10;
	public float popRadius = 1;
	public AudioClip popAudio;
	public Transform popParticles;
	public ItemData hatData;
	int tapCount;
	Rigidbody2D rig;
	Vector3 startScale;
	int currentHat;

	void Start()
	{
		rig = GetComponent<Rigidbody2D>();
		startScale = transform.localScale;
		currentHat = Random.Range(0, hatData.items.Length);
		if (currentHat < hatData.items.Length)
		{
			ItemData.Item data = hatData.items[currentHat];
			if (ProgressManager.ItemOwned(data.dataName))
			{
				SpriteRenderer hatRend = transform.GetChild(0).GetComponent<SpriteRenderer>();
				hatRend.sprite = data.sprite;
			}
			else
			{
				currentHat = hatData.items.Length;
			}
		}
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		transform.localScale *= 1.3f;
		tapCount++;
	}

	void FixedUpdate()
	{
		if (tapCount > 0)
		{
			rig.AddForce(Vector2.up * balloonForce * tapCount, ForceMode2D.Force);
		}
		if (tapCount > maxTaps)
		{
			AudioSource.PlayClipAtPoint(popAudio, GetComponent<Rigidbody2D>().position);
			tapCount = 0;
			Explode();
			transform.localScale = startScale;
			PoolBoss.Despawn(transform);
			PigSpawn.pigCount--;

			//add coins
			int coinAdd = 10;
			if (currentHat < hatData.items.Length)
			{
				coinAdd = (int) (coinAdd * hatData.items[currentHat].coinMultiplier);
			}
			ProgressManager.AddCoins(coinAdd);
		}
	}

	void Explode()
	{
		PoolBoss.Spawn(popParticles, rig.position, Quaternion.identity, null);
		Collider2D[] colliders = Physics2D.OverlapCircleAll(rig.position, popRadius);
		foreach (Collider2D col in colliders)
		{
			Rigidbody2D body = col.GetComponent<Rigidbody2D>();
			if (body == null)
			{
				continue;
			}
			float distance = Vector2.Distance(rig.position, body.position);
			float force = Mathf.Lerp(popForce, 0, distance / popRadius);
			body.AddForce(Vector2.up * force, ForceMode2D.Impulse);
		}
	}
}
