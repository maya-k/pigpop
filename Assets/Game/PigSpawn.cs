﻿using UnityEngine;
using System.Collections;

public class PigSpawn : MonoBehaviour {

	public Transform pigPrefab;
	[Range(0, 3f)]
	public float spawnRange;
	public int maxPigs = 20;
	public float spawnTime = 0.5f;
	public float initialSpawnTime = 0.1f;
	public static int pigCount;
	float timer;
	bool doneInitial;

	void Awake()
	{
		pigCount = 0;
	}

	void Update()
	{
		//check for upgrades
		{
			if (ProgressManager.ItemOwned("upgrade-pigs-tier2"))
			{
				maxPigs = 40;
			}
		}

		if (pigCount >= maxPigs)
		{
			doneInitial = true;
			return;
		}
		timer += Time.deltaTime;
		float time = initialSpawnTime;
		if (doneInitial)
		{
			time = spawnTime;
		}
		if (timer >= time)
		{
			timer = 0;
			SpawnPig();
		}
	}

	void SpawnPig()
	{
		GetComponent<AudioSource>().Play();
		pigCount++;
		Vector3 spawnPos = transform.position;
		spawnPos.x = Random.Range(-spawnRange, spawnRange);
		Quaternion spawnRot = Quaternion.Euler(0, 0, Random.value * 360);
		Transform pig = PoolBoss.Spawn(pigPrefab, spawnPos, spawnRot, null);
		Vector2 spawnVelocity = Random.insideUnitCircle - Vector2.up;
		pig.GetComponent<Rigidbody2D>().velocity = spawnVelocity * 5;
	}

	void OnDrawGizmos()
	{
		Gizmos.color = Color.magenta;
		Gizmos.DrawLine(transform.position + Vector3.left * spawnRange, transform.position + Vector3.right * spawnRange);
	}
}
