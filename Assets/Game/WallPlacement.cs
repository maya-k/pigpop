﻿using UnityEngine;
using System.Collections;

public class WallPlacement : MonoBehaviour {

	void Start()
	{
		float ratio = Screen.width / (float)Screen.height;
		float sidePosition = ratio * 3;
		Vector3 startPos = transform.position;
		if (startPos.x < 0)
		{
			transform.position = Vector3.left * (sidePosition + 0.5f);
		}
		else
		{
			transform.position = Vector3.right * (sidePosition + 0.5f);
		}
	}
}
