﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CoinsDisplay : MonoBehaviour {

	Text text;
	int displayCoins;

	// Use this for initialization
	void Start () {
		text = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		int coins = ProgressManager.CurrentCoins;
		displayCoins = (displayCoins + coins + 1) / 2;
		if (displayCoins > 0)
		{
			text.text = displayCoins + "c";
		}
		else
		{
			text.text = "";
		}
	}
}
