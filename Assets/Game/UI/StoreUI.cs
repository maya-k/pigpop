﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StoreUI : MonoBehaviour {

	public ItemData hatData;
	public ItemData upgradeData;
	public ItemData moneyItemData;

	public GameObject hatItem;
	public GameObject upgradeItem;
	public GameObject moneyItem;

	ItemData.Item offeredHat;
	ItemData.Item offeredUpgrade;
	ItemData.Item offeredMoneyItem;

	void Start()
	{
		RefreshHats();
		RefreshUpgrades();
		RefreshMoneyItems();
	}

	void RefreshHats()
	{
		offeredHat = null;
		foreach (ItemData.Item hat in hatData.items)
		{
			if (!ProgressManager.ItemOwned(hat.dataName))
			{
				offeredHat = hat;
				SetItem(hatItem, hat.displayName, hat.cost);
				return;
			}
		}
		hatItem.SetActive(false);
	}

	void RefreshUpgrades()
	{
		offeredUpgrade = null;
		foreach (ItemData.Item upgrade in upgradeData.items)
		{
			if (!ProgressManager.ItemOwned(upgrade.dataName))
			{
				offeredUpgrade = upgrade;
				SetItem(upgradeItem, upgrade.displayName, upgrade.cost);
				return;
			}
		}
		upgradeItem.SetActive(false);
	}

	void RefreshMoneyItems()
	{
		offeredMoneyItem = null;
		foreach (ItemData.Item premium in moneyItemData.items)
		{
			if (!ProgressManager.ItemOwned(premium.dataName))
			{
				offeredMoneyItem = premium;
				SetItem(moneyItem, premium.displayName, premium.cost);
				return;
			}
		}
		moneyItem.SetActive(false);
	}

	void SetItem(GameObject item, string itemName, int price)
	{
		Text itemText = item.GetComponent<Text>();
		itemText.text = itemName;
		Text priceText = item.transform.GetChild(0).GetComponentInChildren<Text>();
		priceText.text = string.Format("Buy {0}c", price);
	}

	public void PurchaseHat()
	{
		if (offeredHat != null)
		{
			ProgressManager.PurchaseItem(offeredHat.dataName, offeredHat.cost);
			RefreshHats();
		}
	}

	public void PurchaseUpgrade()
	{
		if (offeredUpgrade != null)
		{
			ProgressManager.PurchaseItem(offeredUpgrade.dataName, offeredUpgrade.cost);
			RefreshUpgrades();
		}
	}

	public void PurchaseMoneyItem()
	{
		if (offeredMoneyItem != null)
		{
			ProgressManager.PurchaseItem(offeredMoneyItem.dataName, offeredMoneyItem.cost);
			RefreshMoneyItems();
		}
	}
}
