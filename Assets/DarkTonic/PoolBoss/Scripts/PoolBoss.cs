using UnityEngine;
using System.Collections;
using System.Collections.Generic;

    /// <summary>
    /// This class handles all spawning and despawning
    /// </summary>
public class PoolBoss : MonoBehaviour {
    private const string SPAWNED_MESSAGE_NAME = "OnSpawned";
    private const string DESPAWNED_MESSAGE_NAME = "OnDespawned";

    public List<PoolBossItem> poolItems = new List<PoolBossItem>();
    public bool logMessages = false;
    public bool poolItemsExpanded = true;
    public bool autoAddMissingPoolItems = false;

    private static Dictionary<string, PoolItemInstanceList> poolItemsByName = new Dictionary<string, PoolItemInstanceList>();
    private static Transform _trans;
    private static PoolBoss _instance;

    public class PoolItemInstanceList {
        public bool _logMessages = false;
        public bool _allowInstantiateMore = false;
        public int? _itemHardLimit = null;
        public Transform _sourceTrans = null;
        public List<Transform> _spawnedClones = new List<Transform>();
        public List<Transform> _despawnedClones = new List<Transform>();

        public PoolItemInstanceList(List<Transform> clones) {
            _spawnedClones.Clear();
            _despawnedClones = clones;
        }
    }

    public static PoolBoss Instance {
        get {
            if (_instance == null) {
                _instance = (PoolBoss)GameObject.FindObjectOfType(typeof(PoolBoss));
            }

            return _instance;
        }
    }

    void Awake() {
        _trans = this.transform;

        poolItemsByName.Clear();

        for (var p = 0; p < poolItems.Count; p++) {
            var item = poolItems[p];

            if (item.instancesToPreload <= 0) {
                continue;
            }

            if (item.prefabTransform == null) {
                Debug.LogError("You have an item in Pool Boss with no prefab assigned at position: " + (p + 1));
                continue;
            }

            var itemName = item.prefabTransform.name;
            if (poolItemsByName.ContainsKey(itemName)) {
                Debug.LogError("You have more than one instance of '" + itemName + "' in Pool Boss. Skipping the second instance.");
                continue;
            }

            var itemClones = new List<Transform>();

            for (var i = 0; i < item.instancesToPreload; i++) {
                var createdObjTransform = InstantiateForPool(item.prefabTransform, i + 1);
                itemClones.Add(createdObjTransform);
            }

            var instanceList = new PoolItemInstanceList(itemClones);
            instanceList._logMessages = item.logMessages;
            instanceList._allowInstantiateMore = item.allowInstantiateMore;
            instanceList._sourceTrans = item.prefabTransform;
            instanceList._itemHardLimit = item.itemHardLimit;

            poolItemsByName.Add(itemName, instanceList);
        }
    }

    private static Transform InstantiateForPool(Transform prefabTrans, int cloneNumber) {
        var createdObjTransform = GameObject.Instantiate(prefabTrans, _trans.position, prefabTrans.rotation) as Transform;
        createdObjTransform.name = prefabTrans.name + " (Clone " + cloneNumber + ")"; // don't want the "(Clone)" suffix.
        createdObjTransform.parent = _trans;
        SetActive(createdObjTransform.gameObject, false);

        return createdObjTransform;
    }

    private static void CreateMissingPoolItem(Transform missingTrans, string itemName, bool isSpawn) {
        var instances = new List<Transform>();

        if (isSpawn) {
            var createdObjTransform = InstantiateForPool(missingTrans, instances.Count + 1);
            instances.Add(createdObjTransform);
        }
        var newItemSettings = new PoolItemInstanceList(instances);

        newItemSettings._logMessages = false;
        newItemSettings._allowInstantiateMore = true;
        newItemSettings._sourceTrans = missingTrans;

        poolItemsByName.Add(itemName, newItemSettings);

        // for the Inspector only
        PoolBoss.Instance.poolItems.Add(new PoolBossItem() {
            instancesToPreload = 1,
            isExpanded = true,
            allowInstantiateMore = true,
            logMessages = false,
            prefabTransform = missingTrans
        });

        if (PoolBoss.Instance.logMessages) {
            Debug.LogWarning("PoolBoss created Pool Item for missing item '" + itemName + "' at " + Time.time);
        }
    }

    /// <summary>
    /// This method will spawn an instance of the prefab you pass in as a top-level Game Object with no parent.
    /// </summary>
    /// <param name="transToSpawn">This is the prefab you wish to spawn.</param>
    /// <param name="position">This is the position it will spawn at.</param>
    /// <param name="rotation">This is the rotation it will spawn with.</param>
    /// <returns>A reference to the object that was spawned (null if nothing spawned)</returns>
    public static Transform SpawnOutsidePool(Transform transToSpawn, Vector3 position, Quaternion rotation) {
        return Spawn(transToSpawn, position, rotation, null);
    }

    /// <summary>
    /// This method will spawn an instance of the prefab you pass in as a child of the Pool Boss prefab in the Scene.
    /// </summary>
    /// <param name="transToSpawn">This is the prefab you wish to spawn.</param>
    /// <param name="position">This is the position it will spawn at.</param>
    /// <param name="rotation">This is the rotation it will spawn with.</param>
    /// <returns>A reference to the object that was spawned (null if nothing spawned)</returns>
    public static Transform SpawnInPool(Transform transToSpawn, Vector3 position, Quaternion rotation) {
        return Spawn(transToSpawn, position, rotation, Trans);
    }

    public static Transform Spawn(Transform transToSpawn, Vector3 position, Quaternion rotation, Transform parentTransform) {
        if (transToSpawn == null) {
            Debug.LogError("No Transform passed to Spawn method.");
            return null;
        }

        if (Instance == null) {
            return null;
        }

        var itemName = transToSpawn.name;
        if (!poolItemsByName.ContainsKey(itemName)) {
            if (PoolBoss.Instance.autoAddMissingPoolItems) {
                CreateMissingPoolItem(transToSpawn, itemName, true);
            } else {
                Debug.LogError("The Transform '" + itemName + "' passed to Spawn is not in the Pool Boss.");
                return null;
            }
        }

        var itemSettings = poolItemsByName[itemName];

        if (itemSettings._despawnedClones.Count == 0) {
            if (!itemSettings._allowInstantiateMore) {
                Debug.LogError("The Transform '" + transToSpawn.name + "' has no available clones left to Spawn in the Pool Boss. Please increase your Preload Qty or turn on Allow Instantiate More.");
                return null;
            } else {
                // Instantiate a new one
                var curCount = NumberOfClones(itemSettings);
                if (curCount >= itemSettings._itemHardLimit) {
                    Debug.LogError("The Transform '" + transToSpawn.name + "' has reached its item limit in the Pool Boss. Please increase your Preload Qty or Item Limit.");
                    return null;
                }

                var createdObjTransform = InstantiateForPool(itemSettings._sourceTrans, curCount + 1);
                itemSettings._despawnedClones.Add(createdObjTransform);

                if (PoolBoss.Instance.logMessages || itemSettings._logMessages) {
                    Debug.LogWarning("PoolBoss Instantiated an extra '" + itemName + "' at " + Time.time + " because there were none left in the Pool.");
                }
            }
        }

        var randomIndex = UnityEngine.Random.Range(0, itemSettings._despawnedClones.Count);
        var cloneToSpawn = itemSettings._despawnedClones[randomIndex];

        if (cloneToSpawn == null) {
            Debug.LogError("One or more of the prefab '" + itemName + "' in PoolBoss has been destroyed. You should never destroy objects in the Pool. Despawn instead. Not spawning anything for this call.");
            return null;
        }
        cloneToSpawn.position = position;
        cloneToSpawn.rotation = rotation;
        SetActive(cloneToSpawn.gameObject, true);

        if (PoolBoss.Instance.logMessages || itemSettings._logMessages) {
            Debug.Log("PoolBoss spawned '" + itemName + "' at " + Time.time);
        }

        cloneToSpawn.parent = parentTransform;

        cloneToSpawn.BroadcastMessage(SPAWNED_MESSAGE_NAME, SendMessageOptions.DontRequireReceiver);

        itemSettings._despawnedClones.Remove(cloneToSpawn);
        itemSettings._spawnedClones.Add(cloneToSpawn);

        return cloneToSpawn;
    }

    /// <summary>
    /// This method will despawn the Game Object of the Transform you pass in.
    /// </summary>
    /// <param name="transToDespawn"></param>
    public static void Despawn(Transform transToDespawn) {
        if (!IsActive(transToDespawn.gameObject)) {
            return; // already sent to despawn
        }

        if (PoolBoss.Instance == null) {
            // Scene changing, do nothing.
            return;
        }

        if (transToDespawn == null) {
            Debug.LogError("No Transform passed to Despawn method.");
            return;
        }

        var itemName = GetPrefabName(transToDespawn);

        if (!poolItemsByName.ContainsKey(itemName)) {
            if (PoolBoss.Instance.autoAddMissingPoolItems) {
                CreateMissingPoolItem(transToDespawn, itemName, false);
            } else {
                Debug.LogError("The Transform '" + itemName + "' passed to Despawn is not in the Pool Boss. Not despawning.");
                return;
            }
        }

        transToDespawn.BroadcastMessage(DESPAWNED_MESSAGE_NAME, SendMessageOptions.DontRequireReceiver);

        var cloneList = poolItemsByName[itemName];

        transToDespawn.parent = _trans;
        SetActive(transToDespawn.gameObject, false);

        if (PoolBoss.Instance.logMessages || cloneList._logMessages) {
            Debug.Log("PoolBoss despawned '" + itemName + "' at " + Time.time);
        }

        cloneList._spawnedClones.Remove(transToDespawn);
        cloneList._despawnedClones.Add(transToDespawn);
    }

    /// <summary>
    /// This method will despawn all spawned prefabs.
    /// </summary>
    public static void DespawnAllPrefabs() {
        if (PoolBoss.Instance == null) {
            // Scene changing, do nothing.
            return;
        }

        var items = poolItemsByName.Values.GetEnumerator();
        while (items.MoveNext()) {
            DespawnAllOfPrefab(items.Current._sourceTrans);
        }
    }

    /// <summary>
    /// This method will despawn all spawned instances of the prefab you pass in.
    /// </summary>
    /// <param name="transToDespawn"></param>
    public static void DespawnAllOfPrefab(Transform transToDespawn) {
        if (PoolBoss.Instance == null) {
            // Scene changing, do nothing.
            return;
        }

        if (transToDespawn == null) {
            Debug.LogError("No Transform passed to DespawnAllOfPrefab method.");
            return;
        }

        var itemName = GetPrefabName(transToDespawn);

        if (!poolItemsByName.ContainsKey(itemName)) {
            Debug.LogError("The Transform '" + itemName + "' passed to DespawnAllOfPrefab is not in the Pool Boss. Not despawning.");
            return;
        }

        var spawned = poolItemsByName[itemName]._spawnedClones;

        var max = spawned.Count;
        while (spawned.Count > 0 && max > 0) {
            Despawn(spawned[0]);
            max--;
        }
    }

    /// <summary>
    /// Call this method get info on a Pool Boss item (number of spawned and despawned copies, allow instantiate more, log etc).
    /// </summary>
    /// <param name="poolItemName">The name of the prefab you're asking about.</param>
    public static PoolItemInstanceList PoolItemInfoByName(string poolItemName) {
        if (string.IsNullOrEmpty(poolItemName)) {
            return null;
        }

        if (!poolItemsByName.ContainsKey(poolItemName)) {
            return null;
        }

        return poolItemsByName[poolItemName];
    }

    /// <summary>
    /// Call this method determine if the item (Transform) you pass in is set up in Pool Boss.
    /// </summary>
    /// <param name="trans">Transform you want to know is in the Pool or not.</param>
    public static bool PrefabIsInPool(Transform trans) {
        return PrefabIsInPool(trans.name);
    }

    /// <summary>
    /// Call this method determine if the item name you pass in is set up in Pool Boss.
    /// </summary>
    /// <param name="string">Item name you want to know is in the Pool or not.</param>
    public static bool PrefabIsInPool(string transName) {
        return poolItemsByName.ContainsKey(transName);
    }

    /// <summary>
    /// This method will tell you how many different items are set up in Pool Boss.
    /// </summary>
    public static int PrefabCount {
        get {
            return poolItemsByName.Count;
        }
    }

    private static int NumberOfClones(PoolItemInstanceList instList) {
        return instList._despawnedClones.Count + instList._spawnedClones.Count;
    }

    private static string GetPrefabName(Transform trans) {
        if (trans == null) {
            return null;
        }

        var itemName = trans.name;
        var iParen = itemName.IndexOf(" (");
        if (iParen > -1) {
            itemName = itemName.Substring(0, iParen);
        }

        return itemName;
    }

    /// <summary>
    /// This is a cross-Unity-version method to tell you if a GameObject is active in the Scene.
    /// </summary>
    /// <param name="go">The GameObject you're asking about.</param>
    /// <returns>True or false</returns>
    public static bool IsActive(GameObject go) {
#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5
        return go.active;
#else
		return go.activeInHierarchy;
#endif
    }

    /// <summary>
    /// This is a cross-Unity-version method to set a GameObject to active in the Scene.
    /// </summary>
    /// <param name="go">The GameObject you're setting to active or inactive</param>
    /// <param name="isActive">True to set the object to active, false to set it to inactive.</param>
    public static void SetActive(GameObject go, bool isActive) {
#if UNITY_3_0 || UNITY_3_1 || UNITY_3_2 || UNITY_3_3 || UNITY_3_4 || UNITY_3_5
        go.SetActiveRecursively(isActive);
#else
		go.SetActive(isActive);
#endif
    }

    public static Transform Trans {
        get {
            if (_trans == null) {
                _trans = Instance.GetComponent<Transform>();
            }

            return _trans;
        }
    }
}