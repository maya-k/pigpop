using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public static class DTPoolBossInspectorUtility {
	private const string FOLD_OUT_TOOLTIP = "Click to expand or collapse";
    private const int CONTROLS_DEFAULT_LABEL_WIDTH = 140;
	
	public enum FunctionButtons { None, Add, Remove, ShiftUp, ShiftDown, Edit, DespawnAll, Rename }
	
    public static FunctionButtons AddFoldOutListItemButtons(int position, int totalPositions, string itemName, bool showAddButton, bool showMoveButtons = false)
    {
		if (Application.isPlaying) {
			return FunctionButtons.None;
		}

		if (showMoveButtons) {
			if (position > 0) {
				// the up arrow.
				var upArrow = PoolBossInspectorResources.upArrowTexture;
        		if (GUILayout.Button(new GUIContent(upArrow, "Click to shift " + itemName + " up"),
                                          EditorStyles.toolbarButton, GUILayout.Width(24))) {
					return FunctionButtons.ShiftUp;
				}
			} else {
				GUILayout.Space(24);
			}

			if (position < totalPositions - 1) {
	        	// The down arrow will move things towards the end of the List
				var dnArrow = PoolBossInspectorResources.downArrowTexture;
	        	if (GUILayout.Button(new GUIContent(dnArrow, "Click to shift " + itemName + " down"), 
					EditorStyles.toolbarButton, GUILayout.Width(24))) {
					
					return FunctionButtons.ShiftDown;
				}
			} else {
				GUILayout.Space(24);
			}
		}
		
		if (showAddButton) {
			GUI.contentColor = Color.yellow;

			var addPress = false;
			if (GUILayout.Button(new GUIContent("Add", "Click to insert " + itemName),
	                                              EditorStyles.toolbarButton, GUILayout.Width(32))) {
				addPress = true;
			}

			GUI.contentColor = Color.white;

			if (addPress) {
				return FunctionButtons.Add;
			}
		}
		
		// Remove Button - Process presses later
		var removeContent = PoolBossInspectorResources.deleteTexture == null ? new GUIContent("-", "Click to remove " + itemName) : new GUIContent(PoolBossInspectorResources.deleteTexture, "Click to remove " + itemName);
		
		if (GUILayout.Button(removeContent, EditorStyles.toolbarButton, GUILayout.Width(32))) {
			return FunctionButtons.Remove;
		}

        return FunctionButtons.None;
    }

	public static bool Foldout(bool expanded, string label)
    {
        var content = new GUIContent(label, FOLD_OUT_TOOLTIP);
        expanded = EditorGUILayout.Foldout(expanded, content);

        return expanded;
    }
	
	public static void DrawTexture(Texture tex) {
		if (tex == null) {
			Debug.Log("Logo texture missing");
			return;
		}
		
		Rect rect = GUILayoutUtility.GetRect(0f, 0f);
		rect.width = tex.width;
		rect.height = tex.height;
		GUILayout.Space(rect.height);
		GUI.DrawTexture(rect, tex);
	}
	
	public static void ShowColorWarning(string warningText) {
		GUI.color = Color.green;
		EditorGUILayout.LabelField(warningText, EditorStyles.miniLabel);
		GUI.color = Color.white;
	}

	public static void ShowRedError(string errorText) {
		GUI.color = Color.red;
		EditorGUILayout.LabelField(errorText, EditorStyles.toolbarButton);
		GUI.color = Color.white;
	}

	public static void ShowLargeBarAlert(string errorText) {
		GUI.color = Color.yellow;
		EditorGUILayout.LabelField(errorText, EditorStyles.toolbarButton);
		GUI.color = Color.white;
	}
	
	private static PrefabType GetPrefabType(Object gObject) {
		return PrefabUtility.GetPrefabType(gObject);
	}
	
	public static bool IsPrefabInProjectView(Object gObject) {
		return GetPrefabType(gObject) == PrefabType.Prefab;
	}
}
