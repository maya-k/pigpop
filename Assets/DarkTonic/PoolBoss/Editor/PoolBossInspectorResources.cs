using UnityEngine;
using System.Collections;
using UnityEditor;

public static class PoolBossInspectorResources {
	public const string PoolBossFolderPath = "PoolBoss";

	//public static Texture logoTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/inspector_header_killer_waves.png", CoreGameKitFolderPath)) as Texture;
	public static Texture deleteTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/deleteIcon.png", PoolBossFolderPath)) as Texture;
	public static Texture leftArrowTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/arrow_left.png", PoolBossFolderPath)) as Texture;
	public static Texture rightArrowTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/arrow_right.png", PoolBossFolderPath)) as Texture;
	public static Texture upArrowTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/arrow_up.png", PoolBossFolderPath)) as Texture;
	public static Texture downArrowTexture = EditorGUIUtility.LoadRequired(string.Format("{0}/arrow_down.png", PoolBossFolderPath)) as Texture;
}
