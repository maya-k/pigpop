﻿using UnityEngine;
using System.Collections;

public class DeviceSetup : MonoBehaviour {

	static bool doneSetup = false;

	void Awake()
	{
		if (doneSetup)
		{
			return;
		}
		doneSetup = true;

		Application.targetFrameRate = 60;
	}
}
