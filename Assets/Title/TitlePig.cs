﻿using UnityEngine;
using System.Collections;

public class TitlePig : MonoBehaviour {

	public Animator[] textAnimators;
	public ParticleSystem particles;
	public float wiggleSpeed = 1;
	public float wiggleStrength = 15;
	bool pressed;
	SpriteRenderer rend;
	float wiggleTimer;

	void Start()
	{
		rend = GetComponent<Renderer>() as SpriteRenderer;
		wiggleSpeed *= Mathf.PI;
	}

	void Update () {
		wiggleTimer += Time.deltaTime * wiggleSpeed;
		float wiggle = Mathf.Cos(wiggleTimer) * wiggleStrength;
		transform.localEulerAngles = Vector3.forward * wiggle;
	}

	void OnMouseDown()
	{
		if (pressed)
		{
			return;
		}
		iTween.ScaleTo(gameObject, Vector3.one * 0.8f, 0.4f);
	}

	void OnMouseUp()
	{
		if (pressed)
		{
			return;
		}
		foreach (Animator anim in textAnimators)
		{
			anim.SetBool("Hide", true);
		}

		pressed = true;
		{	Hashtable args = new Hashtable();
			args.Add("from", Color.white);
			args.Add("to", new Color(1, 1, 1, 0));
			args.Add("time", 0.075f);
			args.Add("delay", 0.025f);
			args.Add("easetype", iTween.EaseType.easeOutCubic);
			args.Add("onupdate", "OnColorUpdated");
			
			iTween.ValueTo(rend.gameObject, args);
		}
		{
			Hashtable args = new Hashtable();
			args["scale"] = Vector3.one * 1.8f;
			args["time"] = 0.1f;
			args.Add("easetype", iTween.EaseType.easeOutCubic);
			args["oncomplete"] = "PopComplete";
			iTween.ScaleTo(gameObject, args);
		}
		GetComponent<AudioSource>().Play();
		particles.Play();
	}

	void OnColorUpdated(Color color)
	{
		rend.color = color;
	}
	
	void PopComplete()
	{
		Invoke("LoadLevel", 0.75f);
	}

	void LoadLevel()
	{
		Application.LoadLevel("Game");
	}
}
