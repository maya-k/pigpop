﻿using UnityEngine;
using System.Collections;

public class MusicPlayer : MonoBehaviour {

	static MusicPlayer _instance;

	public float fadeTime;
	float fadeTimer;
	float startVolume;
	AudioClip nextTrack;

	void Awake() {
		if (_instance)
		{
			Destroy(gameObject);
			return;
		}
		_instance = this;
		DontDestroyOnLoad(gameObject);
		startVolume = GetComponent<AudioSource>().volume;
	}

	void Update () {
		if (nextTrack != null)
		{
			fadeTimer -= Time.deltaTime;
			float volumeMultiplier = fadeTimer / fadeTime;
			GetComponent<AudioSource>().volume = volumeMultiplier * startVolume;
			if (fadeTimer <= 0)
			{
				GetComponent<AudioSource>().clip = nextTrack;
				GetComponent<AudioSource>().Play();
				nextTrack = null;
				GetComponent<AudioSource>().volume = startVolume;
			}
		}
	}

	public static void PlayTrack(AudioClip track)
	{
		_instance.nextTrack = track;
		_instance.fadeTimer = _instance.fadeTime;
	}
}
