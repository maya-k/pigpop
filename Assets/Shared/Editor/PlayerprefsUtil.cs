﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PlayerprefsUtil {

	[MenuItem("Tools/Clear Playerprefs")]
	static void ClearPrefs()
	{
		PlayerPrefs.DeleteAll();
	}

}
